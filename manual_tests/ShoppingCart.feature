Feature: Shopping Cart
  The shopping cart is an integral part of the e-commerce experience.
  It's through it that customers select which items they want to check out (and buy)

  Background:
    Given I visit http://automationpractice.com
    And I sign in with a valid, previously created, account

  Scenario: Successfully add an item to the shopping cart from quick view (results page)
    Given I search for an item using the keyword "dress"
    When I select the option "Add to Cart"
    Then a pop-up should be displayed on the same window with the following message:
      | message | Product successfully added to your shopping cart |
    And every information of the added item is displayed
    And the "Cart" button display that I have "1 product"

  Scenario: Successfully add an item to the shopping cart from the product page
    Given I search for an item using the keyword "dress"
    And I visit one of the search results product page
    When I select the option "Add to Cart"
    Then a pop-up should be displayed on the same window with the following message:
      | message | Product successfully added to your shopping cart |
    And every information of the added item is displayed
    And the "Cart" button display that I have "1 product"

  Scenario: Successfully add multiple items to the shopping cart from the product page
    Given I search for an item using the keyword "dress"
    And I visit one of the search results product page
    And I change the "Quantity" to 3
    When I select the option "Add to Cart"
    Then a pop-up should be displayed on the same window with the following message:
      | message | Product successfully added to your shopping cart |
    And every information of the added item is displayed
    And the "Cart" button display that I have "3 products"

  Scenario: Verify if item is persisted in the shopping cart after leaving the shopping cart screen
    Given I have an item in the shopping cart
    And I am the shopping cart screen
    When I go back to the home page
    Then the item should still be listed on the Cart drop down button

  Scenario: Successfully add another of the same item from shopping cart screen
    Given I have a single item in the shopping cart
    And I am at the shopping cart page
    When I select the "+" button on the Qty frame
    Then the item quantity should change to 2
    And the Total Price of the products should reflect the added price of another item

  Scenario: Successfully completely remove an item from shopping cart screen using the "-" button
    Given I have a single item in the shopping cart
    And I am at the shopping cart page
    When I select the "-" button on the Qty frame
    Then the item should be removed from the shopping cart
    And the Total should be set to $0,00

  Scenario: Successfully remove an item from shopping cart screen using the "-" button
    Given I have two of the same item in the shopping cart
    And I am at the shopping cart page
    When I select the "-" button on the Qty frame
    Then the item quantity should change to 1
    And the Total Price of the products should reflect the subtracted price of the removed item

  Scenario: Successfully completely remove items from shopping cart screen using the "Trash Bin" button
    Given I have multiple of the same item in the shopping cart
    And I am at the shopping cart page
    When I select the "Trash Bin" button
    Then all of the selected items should be removed from the shopping cart
    And the Total should be set to $0,00

  Scenario: Successfully remove items from shopping cart using the "X" button from dropdown menu shopping cart on the home page
    Given I have an item in the shopping cart
    When I select the "X" buttom (remove) from the drop down shopping cart button
    Then the selected items should be removed from the shopping cart
    And the Total should be set to $0,00

  Scenario: Verify if items are removed from shopping cart once sign out is performed
    Given I have multiple items in the shopping cart
    And I signed out of my account
    When I sign back in with the same account
    Then the shopping cart should be empty (zero items)