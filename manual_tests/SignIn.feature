Feature: Login into to the YourLogo store
  For registered users, login in into the website allows for adding items into the shopping cart,
  checking out items, checking the status and history of purchases, among others

  Background:
    Given I visit http://automationpractice.com
    And I have a previously registered user
    And I select the option Sign in

  Scenario Outline: Sign in attempts
    Given I fill the email address with <email>
    And I fill the passworld with <password>
    When I click the "Sign in" button
    Then the sign in is <result>

    Examples:
      | email             | password  | result     |
      | test@yourlogo.com | pw123456  | Successful |
      | test@yourlogo.com | blablabla | Failed     |
      | invalid@email.com | pw123456  | Failed     |
      | invalid@email.com | blablabla | Failed     |
      | test@yourlogo     | PW123456  | Failed     |
      | test@yourlogo.com |           | Failed     |
      |                   | pw123456  | Failed     |
      |                   |           | Failed     |

  Scenario: Check if the password field is protected by a mask (not displayed as plain text)
    When I fill the password field
    Then the written text in the passworld field should be masked and not displayed as plain text

  Scenario: Check if the masked password in the passworld field will not be revealed when taken out of the password field
    Given that I fill the password field,
    And I copy the content of the field (by using Ctrl/Command + C)
    When I past the content into a text editor (Notepad)
    Then the content should not have been copied
    And the password should not be revealed

  Scenario: After a succesful sign in, check if pressing back in the browser will not sign out the user
    Given I succesfully sign in
    When I press the "Back" button on my internet browser
    Then I should be returned to the sign in page
    But I should still be logged into the website