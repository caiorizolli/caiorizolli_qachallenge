class Users
  include HTTParty

  def initialize
    @base_url = 'https://jsonplaceholder.typicode.com/'
    @get_users_endpoint = 'users/'
    @register_user = 'posts'
  end

  # Makes a GET request with no parameter and returns API response
  def get_users_list_with_no_param

    path = get_url(@base_url, @get_users_endpoint)

    $logger.info("Sending HTTP request to #{path}")
    api_response_data = HTTParty.get(path)

  end

  # Makes a GET request with parameter and returns API response
  def get_users_list_with_param(param)
    @get_users_endpoint = @get_users_endpoint << param

    get_users_list_with_no_param
  end

  def get_url(base, path)
    URI.join(base, path)
  end

  # Arguments received: title, body, user_id, remove_title_flag
  # Builds json body based on a json model with that information
  def register_a_user(*args)
    path = get_url(@base_url, @register_user)

    request_body = build_request(*args)

    json_body = request_body.to_json

    $logger.info("Sending HTTP request to #{path}")
    api_response_data = HTTParty.post(path, body: json_body, headers: { 'Content-type' => 'application/json'})

    api_response_data
  end

  def build_request(*args)
    # Opens json body sample and update it with test information
    request_body = YAML.load_file(File.join(__dir__, '/support/json_fixtures/posts_request_model.yml'))

    # if remove_title_flag == true
    if args[3] == false
      request_body['title'] = args[0]
    # if remove_title_flag == false
    elsif args[3] == true
      request_body = request_body.slice('userId', 'body')
    end
    request_body['body'] = args[1]
    request_body['userId'] = args[2]


    request_body
  end

end