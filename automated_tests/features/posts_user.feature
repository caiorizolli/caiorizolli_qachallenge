@posts-user-test
Feature: Register a new user via POST
  Using this API, I need to be able to register users providing their ID number,
  title and a body.

  Background:
    Given there are registered users on the data base
    And the application has the /users endpoint available
    And the application has the /posts endpoint available
    And I have an userId retrieved from get/users endpoint response request
    And I have a body for that userId

  @register-user
  Scenario: Register an user based on get users response
    And I have a title information for that userId
    When I make a post request to /posts endpoint with userId, title and body
    Then API Rest status code response is 201
    And I see the payload with all the information and new ID generated for that record

  @register-user-missing-parameter
  Scenario: Trying to register a new user without title
    But I don't input the title parameter into the json body
    When I make a post request to /posts endpoint with userId and body
    Then API Rest status code response is 400
    And I receive the following error message:
      | Title | Title parameter must be declared and must not be empty |

  @register-user-error
  Scenario: Trying to register a new user without title
    But I don't have a title information for that userId
    When I make a post request to /posts endpoint with userId and body
    Then API Rest status code response is 422
    And I receive the following business error message:
      | Title | Title must not be empty |