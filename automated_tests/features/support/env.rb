require 'httparty'
require 'rspec'
require 'valid_email'
require 'valid_email/email_validator'
require 'logger'
require 'cicero'

begin
  require 'byebug'
rescue LoadError => e
  puts e.backtrace
end

LOG_LEVELS = {
    'DEBUG' => Logger::DEBUG,
    'INFO' => Logger::INFO,
    'WARN' => Logger::WARN,
    'ERROR' => Logger::ERROR
}.freeze

LOG_LEVEL = LOG_LEVELS[ENV['LOG_LEVEL']] || Logger::INFO

# logging
$logger = Logger.new(STDOUT)
$logger.level = LOG_LEVEL
$logger.formatter = proc do |severity, datetime, _progname, msg|
  date_format = datetime.strftime('%Y-%m-%d %H:%M:%S')
  "[#{date_format}] #{severity}: #{msg}\n"
end