Given("the application has the \/posts endpoint available") do
  # Nothing to do here.
end

Given(/^I have an userId retrieved from get\/users endpoint response request$/) do
  # Retrieves an user information document
  api_response_data = Users.new.get_users_list_with_param('4')

  # Selects id number to be used in the post body from the previous retrieved document
  @user_id = api_response_data['id']
end

Given("I have a body for that userId") do
  # Generates random data to be used in the post body
  @body = Cicero.words(30)
end

Given("I have a title information for that userId") do
  # Generates random data to be used in the post body
  @title = Cicero.words(5)

  # Indicates if 'title' parameter of the json model body will be removed or not
  @remove_title_flag = false
end

Given("I don't have a title information for that userId") do
  # Sets title value to be equal to null
  @title = nil

  # Indicates if 'title' parameter of the json model body will be removed or not
  @remove_title_flag = false
end

Given("I don't input the title parameter into the json body") do
  # Indicates if 'title' parameter of the json model body will be removed or not
  @remove_title_flag = true
end

When(/^I make a post request to \/posts endpoint with userId(?:, title){0,1} and body$/) do
  # Builds json body based on information previously generated/received
  # and makes a post request with it
  @api_response_data = Users.new.register_a_user(@title, @body, @user_id, @remove_title_flag)
end

Then("I see the payload with all the information and new ID generated for that record") do
  # Validates API response
  expect(@api_response_data['id']).should_not be_nil
  expect(@api_response_data['title']).to eql @title
  expect(@api_response_data['body']).to eql @body
  expect(@api_response_data['userId']).to eql @user_id
end

Then(/^I receive the following (?:business ){0,1}error message:$/) do |table|
  # Validates API error response
  expect(@api_response_data['message']).to eql
end