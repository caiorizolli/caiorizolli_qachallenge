Given("there are registered users on the data base") do
  # Nothing to do here. All data should be saved and avaliable on the database
end

Given("the application has the \/users endpoint available") do
  # Nothing to do here. All data should be saved and avaliable on the database
end

When("I make a GET request with no parameters to that endpoint") do
  # Makes HTTP Get request to "https://jsonplaceholder.typicode.com/users"
  # API respose list is saved for future validation
  @api_response_data = Users.new.get_users_list_with_no_param
end

When("I make a GET request with ID parameter {string} to that endpoint") do |param|
  # Makes HTTP Get request to "https://jsonplaceholder.typicode.com/users"
  # API respose list is saved for future validation
  @api_response_data = Users.new.get_users_list_with_param(param)
end

Then("API Rest status code response is {int}") do |api_status_code|
  # Validates API status code
  expect(@api_response_data.code).to eql api_status_code
end

Then('I get a list of customers and their information such as:') do |table|
  # Validates service's precondition:
  #   Every user should have name, username and email
  #   Every email must be valid
  #   User's company's catchphrase should have less than 50 characters

  (0...@api_response_data.length).each do |cnt|
    expect(@api_response_data[cnt]['name']).should_not be_nil
    expect(@api_response_data[cnt]['username']).should_not be_nil
    expect(@api_response_data[cnt]['email']).should_not be_nil
    expect(ValidateEmail.valid?(@api_response_data[cnt]['email'])).to eql true
    expect(@api_response_data[cnt]['company']['catchPhrase'].length).to be < 50
  end
end

Then('I get a single entry of a customer and his\/hers information such as:') do |table|
  # Validates service's precondition:
  #   Every user should have name, username and email
  #   Every email must be valid
  #   User's company's catchphrase should have less than 50 characters

  expect(@api_response_data['name']).should_not be_nil
  expect(@api_response_data['username']).should_not be_nil
  expect(@api_response_data['email']).should_not be_nil
  expect(ValidateEmail.valid?(@api_response_data['email'])).to eql true
  expect(@api_response_data['company']['catchPhrase'].length).to be < 50
end

