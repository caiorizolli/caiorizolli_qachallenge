@get-users-test
Feature: Get user information
  The database will consist of inumerous registered users,
  which may or may note have all it's information filled.
  I need to be able to receive specific users and their information.

  Background:
    Given there are registered users on the data base
    And the application has the /users endpoint available

  @get-user-list-info
  Scenario: Get a list of users and their information
    When I make a GET request with no parameters to that endpoint
    Then API Rest status code response is 200
    And I get a list of customers and their information such as:
      | name                  |
      | username              |
      | email                 |
      | company's catchphrase |

  @get-single-user-info
  Scenario: Get a single entry using API id parameter
    When I make a GET request with ID parameter "3" to that endpoint
    Then API Rest status code response is 200
    And I get a single entry of a customer and his/hers information such as:
      | name                  |
      | username              |
      | email                 |
      | company's catchphrase |