# QA CI&T challenge

## Dependencies

* Ruby 2.4.x

## Installation Instructions

#### Install bundler
To get started, install the bundler:

```
  gem install bundler
```

#### Project Installation
Then you should be able to run the following command to prepare your environment:

```
  bundle install
```

### Run on your Machine

The project uses Cucumber to run its tests, you can use the following command to run all tests:
```
  cucumber
```
Or you can run a specific feature test or a specific scenario test. Just run the following command:
```
  cucumber -t @<test-tag>
```
  

### TAGS

Available tags:

- Feature Get Users tag:
    - @get-users-test  
- Scenarios Get Users tag:
    - @get-user-list-info
    - @get-single-user-info
- Feature Posts Users tag:
    - @posts-user-test
- Scenarios Get Users tag:
    - @register-user
    - @register-user-missing-parameter
    - @register-user-error